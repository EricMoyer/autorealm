#FIND_PACKAGE(wxWidgets REQUIRED base core adv gl aui)
FIND_PACKAGE(wxWidgets REQUIRED gl aui)
INCLUDE(${wxWidgets_USE_FILE})

FILE(GLOB source_files *.cpp )

FIND_PACKAGE(Boost 1.39 COMPONENTS system filesystem program_options REQUIRED)
ADD_EXECUTABLE(autorealm ${source_files})

TARGET_LINK_LIBRARIES(autorealm pluginengine renderengine ${wxWidgets_LIBRARIES} ${Boost_SYSTEM_LIBRARY} ${Boost_FILESYSTEM_LIBRARY} ${Boost_PROGRAM_OPTIONS_LIBRARY})

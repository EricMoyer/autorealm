/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012 Morel Bérenger                                               *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#ifndef DRAWER_H
#define DRAWER_H

#include <gui/id.h>

#include <renderEngine/drawable.h>
#include <renderEngine/shape.h>
#include <renderEngine/taglist.h>

namespace Render
{
	class Shape;
	class Point;
}

#include "plugin.h"

class RenderWindow;
class wxMouseEvent;
class wxContextMenuEvent;
class wxCommandEvent;

class Drawer : public Plugin
{
public:
private:
	std::unique_ptr<Renderer> m_selectedRenderer;
	Render::TagList m_tagList;
protected:
	Render::Shape m_shape;
public:
	Drawer(RenderWindow *window, std::unique_ptr<Renderer> r, Render::TagList const &tags);
	Drawer(Drawer const& other);
	virtual ~Drawer(void)throw();

	void addPoint(wxMouseEvent &event);

	void addVertex(Render::Point const &p);

	void moveMouse(wxMouseEvent &event);
	void render(void);

	operator Renderer&(void);
	bool operator==(Render::TagList const& tag)const;
	Render::TagList const getTags(void)const;
	Renderer *create(Render::TagList const& tag)const;
protected:
	void createShape(void);
};

#endif // DRAWER_H

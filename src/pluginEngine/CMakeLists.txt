set(Pluma_LIBRARY /usr/lib/libpluma.so)

FIND_PACKAGE(wxWidgets REQUIRED)
INCLUDE(${wxWidgets_USE_FILE})

FILE(GLOB source_files "*.cpp")

ADD_LIBRARY(pluginengine SHARED ${source_files} )
TARGET_LINK_LIBRARIES(pluginengine ${wxWidgets_LIBRARIES} ${Pluma_LIBRARY})
#TARGET_LINK_LIBRARIES(pluginengine ${Pluma_LIBRARY})

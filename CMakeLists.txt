## general project configuration ##
cmake_minimum_required(VERSION 2.8.3)
enable_language(CXX)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -g")

## versioning and naming ##
project(AutoREALM)
set (AutoREALM_VERSION_MAJOR "3")
set (AutoREALM_VERSION_MINOR "0")
set (AutoREALM_VERSION_BUILD "2")
set (AutoREALM_VERSION_STRING "alpha")
configure_file( "${PROJECT_SOURCE_DIR}/autorealmconfig.h.in" "${PROJECT_BINARY_DIR}/autorealmconfig.h")
include_directories(${PROJECT_BINARY_DIR})

## destination folders
set (out_bin  ${CMAKE_BINARY_DIR}/bin)
set (out_libs ${CMAKE_BINARY_DIR}/lib)
set (out_conf ${CMAKE_BINARY_DIR}/config)
set (out_res  ${CMAKE_BINARY_DIR}/resources)

set (out_res_png       ${out_res}/png)
set (out_plugins       ${out_libs}/plugins)
set (out_conf_toolbars ${out_conf}/toolbars)
set (out_conf_menubar  ${out_conf}/menubar)


## dependencies
include_directories(src)
subdirs(src)

################
# installation #
################
## install binaries
set(EXECUTABLE_OUTPUT_PATH ${out_bin} )
set(LIBRARY_OUTPUT_PATH    ${out_libs})

## install graphical resources ##
file(GLOB PNG ${CMAKE_SOURCE_DIR}/resources/png)
install(DIRECTORY ${PNG} DESTINATION ${out_res})

## Install default configuration ##
file(GLOB CONFIG CONFIG ${CMAKE_SOURCE_DIR}/config/*)
install(DIRECTORY ${CONFIG} DESTINATION ${out_conf})

file(WRITE  ${out_conf}/config 	
	"graphics =${out_res_png}\n"
	"plugins  =${out_plugins}\n"
	"menubar  =${out_conf_menubar}\n"
	"toolbars =${out_conf_toolbars}\n"
	"splash   =${out_res_png}/splash/splash.png\n"
	"\n"
	"border   =polyline\n"
	"filler   =polyline\n"
)

#############
# packaging #
#############
## general stuff
set(CPACK_PACKAGE_NAME autorealm)
set(CPACK_PACKAGE_VERSION "${AutoREALM_VERSION_MAJOR}.${AutoREALM_VERSION_MINOR}.${AutoREALM_VERSION_BUILD}")
## Debian

